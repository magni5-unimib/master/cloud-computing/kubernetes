# Kubernetes assignment

Deployment of a service using Kubernetes with a pod that calls a previously created nginx backend.

## Instructions

Check out the [instructions](instructions.pdf) for more about the rules and the requirements for the assignment.